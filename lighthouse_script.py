import click
import arrow
import subprocess

@click.command()
@click.option('-domain', '--domain', 'domain', help='測試的domain', required=True)
def main(domain):
    date_now = arrow.now('Asia/Taipei').format('YYYYMMDDHHmmss')
    subprocess.call(f"mkdir -p /tmp/{date_now}", shell=True)
    link_dict = {
        "使用者列表": f'{domain}/admin/account_list',
        "使用者行為紀錄": f'{domain}/admin/account_history_list',
        "服務設定": f'{domain}/admin/marketing_line',
        "發送設定列表": f'{domain}/admin/send_setting',
        "活動列表": f'{domain}/admin/activity',
        "新增活動": f'{domain}/admin/activity/add',
        "編輯活動": f'{domain}/admin/activity/edit?id=622',
        "檢視活動": f'{domain}/admin/activity/view?id=590',
        "模板列表": f'{domain}/admin/template',
        "新增模板基本資訊": f'{domain}/admin/template/add?channel=line&step=1',
        "新增模板-內容頁": f'{domain}/admin/template/add?channel=line&step=2',
        "編輯模板-基本資訊": f'{domain}/admin/template/edit?id=L124&channel=LINE&step=1',
        "編輯模板-內容頁": f'{domain}/admin/template/edit?id=L124&channel=LINE&step=2',
        "檢視模板-基本資訊": f'{domain}/admin/template/view?id=L155&channel=LINE&step=1',
        "檢視模板-內容頁": f'{domain}/admin/template/view?id=L155&channel=LINE&step=2',
        "查無此頁": f'{domain}/error',
        "檔案下載-頁面導向中": f'{domain}/admin/download/redirect',
        "檔案下載-驗證成功": f'{domain}/admin/download/success',
        "檔案下載-驗證失敗": f'{domain}/admin/download/fail',
    }

    for key, val in link_dict.items():
        status = subprocess.call(f"lighthouse '{val}' --only-categories=performance --chrome-flags='--headless --no-sandbox --disable-dev-shm-usage' --screenEmulation.disabled --throttling-method=provided --no-emulatedUserAgent --preset desktop --output-path=/tmp/{date_now}/{key}.html", shell=True)
        print(f"{key}的執行狀態為{status}")


if __name__ == "__main__":
    main()